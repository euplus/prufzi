#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt5.QtCore import QThread, QObject, QByteArray, QAbstractListModel, QModelIndex, pyqtSignal, pyqtSlot, pyqtProperty, Qt
from PyQt5.QtWidgets import QApplication, QDesktopWidget
from declarativeview import DeclarativeView
from notificationtimeout import NotificationTimeout
from rc_data import *
import uuid, os.path, logging

Log = logging.getLogger('pruefzi')

class NotificationModel(QAbstractListModel):
	notificationClosed = pyqtSignal(str)
	notificationClosedBody = pyqtSignal(str, str)
	notificationCountChanged = pyqtSignal(int, arguments=['count'])
	hiddenNotificationCountChanged = pyqtSignal(int, arguments=['count'])
	notificationsToggled = pyqtSignal()
	
	def __init__(self, config):
		super(QAbstractListModel, self).__init__()

		self.config = config
		self.notifications = {}
		self.timeouts = {}
		self.notificationsOrder = []
		self.criticalNotificationsOrder = []
		self.minimalTimeout = 7000
		self.running = True

	def roleNames(self):
		roles = {}
		roles[0] = 'application'.encode('utf-8')
		roles[1] = 'icon'.encode('utf-8')
		roles[2] = 'summary'.encode('utf-8')
		roles[3] = 'body'.encode('utf-8')
		roles[4] = 'uid'.encode('utf-8')
		roles[5] = 'critical'.encode('utf-8')
		roles[6] = 'actionkeys'.encode('utf-8')
		roles[7] = 'actionnames'.encode('utf-8')
		roles[8] = 'image'.encode('utf-8')
		roles[9] = 'checkdigit'.encode('utf-8')

		return roles

	@pyqtSlot()
	def toggleNotificationDisabled(self):
		self.beginResetModel();
		self.config.set('window', 'muted', str(not self.notificationDisabled))
		try:
			self.config.write(open(os.path.expanduser('~/.pruefzi.ini'), 'w'))
		except Exception as e:
			#log.exception(e)
			pass
		self.endResetModel();

		# send notification
		icon = 'kabal_s' if self.notificationDisabled else 'kabal'
		text = 'disabled' if self.notificationDisabled else 'enabled'
		hints = {}
		if self.notificationDisabled:
			hints['urgency'] = 2
		self.Notify('Notify', None, icon, 'Check Digit Calculator', 'Notifications are now %s' % (text,), '', [], hints)
		self.notificationsToggled.emit()

	@pyqtProperty(bool)
	def notificationDisabled(self):
		return self.config.getboolean('window', 'muted')

	@pyqtProperty(bool)
	def persistence(self):
		return self.config.getboolean('window', 'persistence')

	def rowCount(self, modix):
		if self.notificationDisabled:
			return len(self.criticalNotificationsOrder)
		else:
			return len(self.notificationsOrder)

	def hiddenNotificationCount(self):
		if self.notificationDisabled:
			return len(self.notificationsOrder) - len(self.criticalNotificationsOrder)
		else:
			return 0

	def data(self, index, role):
		if not index.isValid():
			return None

		notlist = self.criticalNotificationsOrder if self.notificationDisabled else self.notificationsOrder
		if index.row() >= len(notlist):
			return None

		n = notlist[index.row()]

		if role == 0:
			return n['app']
		elif role == 1:
			return n['icon']
		elif role == 8:
			return n['image']
		elif role == 2 or role == Qt.DisplayRole:
			return n['summary']
		elif role == 3:
			return n['body']
		elif role == 4:
			return n['uid']
		elif role == 5:
			return n['critical']
		elif role == 6:
			return n['actionKeys']
		elif role == 7:
			return n['actionNames']
		elif role == 9:
			return n['checkdigit']
		else:
			return None

	@pyqtSlot()
	def closeAllNotifications(self):
		print('Executed closeAllNotifications')
		for k, i in self.notifications.items():
			self.closeNotification(k)


	@pyqtSlot(str)
	def closeNotification(self, quid, qreason = None):
		uid = str(quid)
		print('Executed closeNotification with %s' % (uid,))
		if uid not in self.notifications.keys():
			print('Notification not in list!')
			return None

		curHiddenCount = self.hiddenNotificationCount()
		notlist = self.criticalNotificationsOrder if self.notificationDisabled else self.notificationsOrder
		try:
			idx = list(self.notifications.keys()).index(quid)
		except:
			idx = -1
		if idx != -1:
			self.beginRemoveRows(QModelIndex(), idx, idx);

		n = self.notifications[uid]
		try:
			self.notificationsOrder.remove(n);
			del(self.notifications[uid])
		except:
			print('Notification not in list!')

		try:
			del(self.timeouts[uid])
		except:
			pass
		
		if n['critical']:
			self.criticalNotificationsOrder.remove(n)

		self.notificationClosed.emit(uid)
		self.notificationClosedBody.emit(uid, n['body'])
		notlist = self.criticalNotificationsOrder if self.notificationDisabled else self.notificationsOrder
		if idx != -1:
			self.endRemoveRows();
			self.notificationCountChanged.emit(len(notlist))
		
		if curHiddenCount != self.hiddenNotificationCount():
			self.hiddenNotificationCountChanged.emit(self.hiddenNotificationCount())

	@pyqtSlot(str, str)
	def invokeAction(self, uid, action):
		print('Executed invokeAction with uid %s and action %s' % (uid, action))

	@pyqtSlot(str, str, str, str, str, str, list, dict, int)
	def Notify(self, app, replace, icon, summary, body, checkdigit, actions, hints, timeout = 0):
		try:
			critical = int(hints['urgency']) == 2
		except:
			critical = False
		try:
			transient = bool(hints['transient'])
		except:
			transient = False
		try:
			resident = bool(hints['resident'])
		except:
			resident = False

		# depending on something, we need to set the timeout.
		if resident:
			timeout = -1
		elif self.persistence and not transient:
			timeout = -1
		elif timeout < self.minimalTimeout:
			timeout = self.minimalTimeout

		# do we first have to close another one?
		if replace is not None and len(replace) > 0:
			self.closeNotification(replace)

		# which icon?
		appIcon = icon;
		if appIcon is None or len(appIcon) <= 0:
			appIcon = app

		uid = str(uuid.uuid4())
		# implement counter?
		# Extract image-data
		iconPath = None
		#QImage img = getImageFromHints(hints);
		#if ( img.isNull() ) {
		#	iconPath = "image://icons/" + appIcon;
		#} else {
		#	iconPath = "image://images/" + QString::number(uid);
		#}
		iconPath = 'qrc:///icons/%s.png' % (appIcon, )
		img = None

		actionKeys = []
		actionNames = []

		n = {
			'uid': uid,
			'app': app,
			'icon': iconPath,
			'image': img,
			'summary': summary,
			'body': body,
			'timeout': timeout,
			'critical': critical,
			'actionKeys': actionKeys,
			'actionkeys': actions,
			'actionNames': actionNames,
			'checkdigit': checkdigit
		}

		curHiddenCount = self.hiddenNotificationCount()
		modelChanged = critical or not self.notificationDisabled
		notList = self.criticalNotificationsOrder if self.notificationDisabled else self.notificationsOrder
		if modelChanged:
			self.beginInsertRows(QModelIndex(), len(notList), len(notList))

		self.notificationsOrder.append(n)
		if critical:
			self.criticalNotificationsOrder.append(n)
		self.notifications[uid] = n

		if modelChanged:
			self.endInsertRows()
			self.notificationCountChanged.emit(len(self.notifications))

		if curHiddenCount != self.hiddenNotificationCount():
			self.hiddenNotificationCountChanged.emit(self.hiddenNotificationCount())

		if timeout > 0:
			self.timeouts[uid] = NotificationTimeout(self, uid, timeout)
			self.timeouts[uid].start()

		return uid

class Notification(QThread):

	def __init__(self, config, qmlSource, parent = None):
		super(QThread, self).__init__(parent)

		self.config = config
		self.qmlSource = qmlSource
		self.model = NotificationModel(config)
		self.widgets = []

		self.desktop = QApplication.desktop()
		self.desktop.screenCountChanged.connect(self.screenCountChanged)
		self.desktop.resized.connect(self.repositionWidget)

		app = QApplication.instance()
		app.aboutToQuit.connect(self.model.closeAllNotifications)

		self.screenCountChanged(self.desktop.screenCount())

	def createWidget(self):
		# Widget
		view = DeclarativeView()
		#view.engine().addImageProvider('icons', IconProvider());
		#view.engine().addImageProvider('images', ImageProvider(self.model));
		view.setFlags(Qt.Tool | Qt.X11BypassWindowManagerHint | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.WA_Hover)

		# Export methods
		view.rootContext().setContextProperty('notificationModel', self.model)
		view.rootContext().setContextProperty('Window', view)

		# Load root qml object
		view.setSource(self.qmlSource)

		root = view.rootObject()
		root.toggleNotificationDisabled.connect(self.model.toggleNotificationDisabled)

		return view

	@pyqtSlot(int)
	def screenCountChanged(self, count):
		# independent of this... if we do want to show only 
		# on one screen, do not replicate!
		if not self.config.getboolean('window', 'allscreen'):
			count = 1

		# Remove unneed widgets
		if count < len(self.widgets):
			i = count - 1
			while i < len(self.widgets) - 1:
				self.widgets[i].deleteLater()
				i = i + 1

		elif count > len(self.widgets):
			i = len(self.widgets) - 1
			while i < count - 1:
				self.widgets.append(self.createWidget())
				i = i + 1
			
	
		# Reposition remaining widgets
		i = 0
		for f in self.widgets:
			self.repositionWidget(i)
			i = i + 1
		
		#self.model.setMinimalTimeout(self.config.getint('window', 'mintime'))

	def getTaskbarPosition(self):
		desktop = QDesktopWidget()
		displayRect = desktop.screenGeometry()
		desktopRect = desktop.availableGeometry()
		if desktopRect.height() < displayRect.height():
			if desktopRect.y() > displayRect.y():
				return 'top', displayRect.height() - desktopRect.height()
			else:
				return 'bottom', displayRect.height() - desktopRect.height()
		else:
			if desktopRect.x() > displayRect.x():
				return 'left', desktopRect.height() - displayRect.height()
			else:
				return 'right', desktopRect.height() - displayRect.height()

	@pyqtSlot(int)
	def repositionWidget(self, i):
		if i >= len(self.widgets) or i < 0:
			return

		Log.info('Reposition the widget: %d' % (i,))

		try:
			W = self.widgets[i]
		except:
			W = None
		if W is None:
			return

		screen = QApplication.desktop().screenGeometry(i)
		taskbarCorner, taskbarHeight = self.getTaskbarPosition()

		# settings
		x = self.config.getint('window', 'x')
		y = self.config.getint('window', 'y')
		corner = self.config.get('window', 'corner').lower()
		# take care about the taskbar
		if taskbarCorner == 'top' and corner.startswith('top'):
			y = y + taskbarHeight
		
		if corner == 'topleft':
			corner_opt = Qt.TopLeftCorner
		elif corner == 'topright':
			corner_opt = Qt.TopRightCorner
		elif corner == 'bottomleft':
			corner_opt = Qt.BottomLeftCorner
		elif corner == 'bottomright':
			corner_opt = Qt.BottomRightCorner
		else:
			corner_opt = Qt.TopLeftCorner

		if corner_opt == Qt.TopRightCorner:
			W.setPosition(screen.right() - W.width() - x, screen.top() + y)
		elif corner_opt == Qt.BottomLeftCorner:
			W.setPosition(screen.left() + x, screen.bottom()- W.height() - y)
		elif corner_opt == Qt.BottomRightCorner:
			W.setPosition(screen.right() - W.width() - x, screen.bottom()- W.height() - y)
		else:
			# default is top left corner
			W.setPosition(screen.left() + x, screen.top() + y)

	@pyqtSlot(str, str, str, str, str, float)
	def showNotification(self, app, icon, summary, body, checkdigit, timeout = 7.0):
		self.model.Notify(app, None, icon, summary, body, checkdigit, [], [], int(timeout*1000))
