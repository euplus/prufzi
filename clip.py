#!/usr/bin/python
# -*- coding: utf-8 -*-
# TODO: 
# - Mutex https://code.activestate.com/recipes/474070/
import sys, math, os, datetime
from PyQt5.QtCore import QDateTime, QObject, QUrl, QDir, pyqtSignal, pyqtSlot, Qt, QEvent
from PyQt5.QtQuick import QQuickView
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QDialog, QSystemTrayIcon, QMainWindow, QMenu, QAction
from tkinter import Tk
from configparser import ConfigParser
from ui_settings import Ui_Settings
from notification import Notification
try:
	import clipmutex
except:
	pass
import logging, traceback, ipaddress
try:
	import win32clipboard
except:
	print('Win32 API not available.')
	win32clipboard = None

APP_NAME = 'Pruefzi'
FORMAT = '%(asctime)-15s %(message)s'
Log = logging.getLogger('pruefzi')
logging.basicConfig(filename='pruefzi.log', filemode='w', format=FORMAT)
Log.setLevel(logging.DEBUG)

class PruefziCalculation:

	def __init__(self, original, text, pz=None, pz2=None, data=None):
		self.original = original
		self.cleared = str(text)
		self.pz = pz
		self.pz2 = pz2
		self.result = data

	def parse(self):
		# calculate the different things: 1 and 2 digit.
		self.pz = self.getPz(self.cleared)
		Log.debug('Set 1st cdigit for {:s} to {:s}'.format(self.original, self.pz))
		self.pz2 = self.getPz2(self.cleared)
		Log.debug('Set 2nd cdigit for {:s} to {:s}'.format(self.original, self.pz2))

	def setResult(self, mode):
		data = None
		if mode != Pruefzi.MODE_OFF:
			if mode == Pruefzi.MODE_C:
				data = self.cleared
			elif mode == Pruefzi.MODE_P:
				data = '{:s}'.format(self.pz)
			elif mode == Pruefzi.MODE_P2:
				data = '{:s}'.format(self.pz2)
			elif mode == Pruefzi.MODE_CP:
				data = '{:s}{:s}'.format(self.cleared, self.pz)
			elif mode == Pruefzi.MODE_CP2:
				data = '{:s}{:s}'.format(self.cleared, self.pz2)
			elif mode == Pruefzi.MODE_DSP:
				data = self.original
			else:
				data = self.cleared
			self.result = data
			Log.debug('Set result to {:s}'.format(data))
		else:
			Log.debug('No result is set due to mode offline.')

	def getPz(self, bnr):
		nr = 3
		pz = 0.0
		bnr = bnr.strip()
		bnr = bnr[::-1]
		for z in bnr:
			pz += nr*int(z)
			nr = 3 if nr == 1 else 1
		if pz > 0:
			# get to next 10base
			pz = int((math.ceil(pz/10)*10)-pz)

		if pz == 0:
			pz = '0'
		else:
			pz = str(pz)

		return pz

	def getPz2(self, bnr):
		nr = 31
		pz = 0.0
		bnrWrk = bnr.strip()
		if (len(bnrWrk) % 2) > 0:
			bnrWrk = '0%s' % (bnrWrk,)
		bnrWrk = [''.join(x) for x in zip(*[list(bnrWrk[z::2]) for z in range(2)])]
		bnrWrk.reverse()

		for z in bnrWrk:
			pz += nr*int(z)
			nr = 31 if nr == 17 else 17

		if pz > 0:
			# get to next 10base
			pzRest = pz % 97
			pz = int(97-pzRest)

		if pz < 10:
			pz = '0%s' % (str(pz),)
		elif pz == 0:
			pz = '00'
		else:
			pz = str(pz)
			
		return pz

	def __str__(self):
		return 'Number: {origNumber}\nCD: {checkDigit}\nCD2: {checkDigit2}\n\nTo paste: {clipboardResult}'.format(
			origNumber=self.original, 
			checkDigit=self.pz, 
			checkDigit2=self.pz2, 
			clipboardResult=self.result
		)

class PruefziSettings(QDialog):
	sigConfigChanged = pyqtSignal()

	def __init__(self, parent, config):
		super(PruefziSettings, self).__init__(parent=parent)
		self.config = config
		self.ui = Ui_Settings()
		self.ui.setupUi(self)

		# set default mode items
		self.ui.defaultMode.addItem('Off', 0)
		self.ui.defaultMode.addItem('Bubble only', 1)
		self.ui.defaultMode.addItem('Number itself', 2)
		self.ui.defaultMode.addItem('Check digit', 4)
		self.ui.defaultMode.addItem('Two number check digit', 8)
		self.ui.defaultMode.addItem('Number + Check digit', 16)
		self.ui.defaultMode.addItem('Number + 2 number check digit', 32)

		# set corner modes
		self.ui.dspCorner.addItem('Top Right', 'topright')
		self.ui.dspCorner.addItem('Top Left', 'topleft')
		self.ui.dspCorner.addItem('Bottom right', 'bottomright')
		self.ui.dspCorner.addItem('Bottom Left', 'bottomleft')

		# update values!
		self.ui.charsToRemove.setText(self.config.get('app', 'charsToRemove'))
		self.ui.excludeIPs.setChecked(self.config.getboolean('app', 'excludeIPs'))
		self.ui.spinMin.setValue(self.config.getint('app', 'minLength'))
		self.ui.spinMax.setValue(self.config.getint('app', 'maxLength'))
		self.ui.closeBubble.setChecked(not self.config.getboolean('window', 'persistence'))
		self.ui.bubbleTime.setValue(self.config.getfloat('app', 'bubbleTime'))
		idx = self.ui.defaultMode.findData(self.config.getint('app', 'defaultMode'))
		if idx >= 0:
			self.ui.defaultMode.setCurrentIndex(idx)
		self.ui.dspBbl.setText(self.config.get('app', 'dspBbl'))
		self.ui.labDspBbl.setVisible(False)
		self.ui.dspBbl.setVisible(False)
		idx = self.ui.dspCorner.findData(self.config.get('window', 'corner'))
		if idx >= 0:
			self.ui.dspCorner.setCurrentIndex(idx)
		self.ui.dspAllScreen.setChecked(self.config.getboolean('window', 'allscreen'))

		self.ui.closeBubble.stateChanged.connect(self.closeBubbleStateChanged)
		self.ui.buttonBox.accepted.connect(self.save)

		self.closeBubbleStateChanged(self.ui.closeBubble.checkState())

	@pyqtSlot(int)
	def closeBubbleStateChanged(self, state):
		if state == Qt.Checked:
			self.ui.bubbleTime.setVisible(True)
			self.ui.labBblTme.setVisible(True)
		else:
			self.ui.bubbleTime.setVisible(False)
			self.ui.labBblTme.setVisible(False)

	@pyqtSlot()
	def save(self):
		self.config.set('app', 'charsToRemove', str(self.ui.charsToRemove.text()))
		self.config.set('app', 'excludeIPs', str(self.ui.excludeIPs.isChecked()))
		self.config.set('app', 'minLength', str(self.ui.spinMin.value()))
		self.config.set('app', 'maxLength', str(self.ui.spinMax.value()))
		self.config.set('window', 'persistence', str(not self.ui.closeBubble.isChecked()))
		self.config.set('app', 'bubbleTime', str(self.ui.bubbleTime.value()))
		
		try:
			mode = self.ui.defaultMode.itemData(self.ui.defaultMode.currentIndex())
		except Exception as e:
			Log.exception(e)
			Log.error('Could not save config: %s' % (str(e),))
			raise
		else:
			self.config.set('app', 'defaultMode', str(mode))
		self.config.set('app', 'dspBbl', str(self.ui.dspBbl.text()))

		try:
			corner = self.ui.dspCorner.itemData(self.ui.dspCorner.currentIndex())
		except:
			pass
		else:
			self.config.set('window', 'corner', str(corner))
		self.config.set('window', 'allscreen', str(self.ui.dspAllScreen.isChecked()))

		try:
			self.config.write(open(os.path.expanduser('~/.pruefzi.ini'), 'w'))
		except Exception as e:
			Log.error('Could not save config: %s' % (str(e),))
			log.exception(e)
			raise

		self.sigConfigChanged.emit()
		self.accept()

class Pruefzi(QObject):

	MODE_OFF = 0
	MODE_DSP = 1
	MODE_C = 2
	MODE_P = 4
	MODE_P2 = 8
	MODE_CP = 16
	MODE_CP2 = 32

	MODE_DESC = {
		0: 'Off',
		1: 'Only Bubble',
		2: 'No.',
		4: 'Check',
		8: 'Check 2',
		16: 'No. + Check',
		32: 'No. + Check 2'
	}

	notify = pyqtSignal(str, str, str, str, str, float)

	def __init__(self, app, ti):
		super(QObject, self).__init__()
		self.mainWindow = QMainWindow()
		self.lastNumber = '0'
		self.dontCheck = None
		self.dontCheckOriginal = None
		self.dontCheckUntil = None
		self.app = app
		self.ti = ti
		self.ti.show()
		Log.info('Started the pruefzi tool!')
		self.menuMode = None
		self.cp = True
		self.mode = Pruefzi.MODE_DSP
		self.loadedConfigs = []
		self.config = None
		self.lastCalculation = None
		self.clipboard = QApplication.clipboard()
		self.activeNotifications = []

		self.loadConfig()
		self.setMenu()

		#self.notification = Notification(self.config, QUrl(QDir(QDir().filePath("qml")).filePath("kabal.qml")))
		self.notification = Notification(self.config, QUrl("qrc:///qml/kabal.qml"))
		self.notify.connect(self.notification.showNotification)
		self.notification.model.notificationsToggled.connect(self.notificationsToggled)
		self.notification.model.notificationClosedBody.connect(self.notificationClosed)
		self.notification.start()
		self.clipboard.dataChanged.connect(self.dataChanged)

	def loadConfig(self):
		self.config = ConfigParser()
		if getattr(sys, 'frozen', False):
			# frozen
			basicConfigPath = os.path.dirname(sys.executable)
		else:
			# unfrozen
			basicConfigPath = os.path.dirname(os.path.realpath(__file__))
		self.config.readfp(open(os.path.join(basicConfigPath, 'settings.ini')))
		self.loadedConfigs = self.config.read([os.path.expanduser('~/.pruefzi.ini')])
		self.mode = self.config.getint('app', 'defaultMode')
		# it is never muted on startup!
		if self.config.getboolean('window', 'muted'):
			self.config.set('window', 'muted', str(False))
			try:
				self.config.write(open(os.path.expanduser('~/.pruefzi.ini'), 'w'))
			except Exception as e:
				Log.exception(e)

	@pyqtSlot()
	def reloadConfig(self):
		Log.info('Reload config per trigger.')
		self.loadConfig()

	def setMenu(self):
		menu = QMenu()
		self.menuMode = QAction('Mode: %s' % (Pruefzi.MODE_DESC[self.mode],), self)
		menu.addAction(self.menuMode)
		menu.addSeparator()
		modeMenu = menu.addMenu('Change Mode')
		muteText = 'Mute' if not self.config.getboolean('window', 'muted') else 'Unmute'
		self.actMuted = QAction('Mute', self)
		self.actMuted.triggered.connect(self.toggleNotification)
		menu.addAction(self.actMuted)
		actSettings = QAction('Settings', self)
		actSettings.triggered.connect(self.settings)
		actSettings.setMenuRole(QAction.PreferencesRole)
		menu.addAction(actSettings)
		actAbout = QAction('About Qt', self)
		actAbout.triggered.connect(self.app.aboutQt)
		actAbout.setMenuRole(QAction.AboutQtRole)
		menu.addAction(actAbout)
		actQuit = QAction('Quit', self)
		actQuit.triggered.connect(self.quit)
		actQuit.setMenuRole(QAction.QuitRole)
		menu.addAction(actQuit)

		actMode = QAction('Off', self)
		actMode.triggered.connect(self.setModeOff)
		modeMenu.addAction(actMode)
		actMode = QAction('Bubble only', self)
		actMode.triggered.connect(self.setModeBubble)
		modeMenu.addAction(actMode)
		actMode = QAction('Number itself', self)
		actMode.triggered.connect(self.setModeC)
		modeMenu.addAction(actMode)
		actMode = QAction('Check digit', self)
		actMode.triggered.connect(self.setModeP)
		modeMenu.addAction(actMode)
		actMode = QAction('Two number check digit', self)
		actMode.triggered.connect(self.setModeP2)
		modeMenu.addAction(actMode)
		actMode = QAction('Number + Check digit', self)
		actMode.triggered.connect(self.setModeCP)
		modeMenu.addAction(actMode)
		actMode = QAction('Number + 2 number check digit', self)
		actMode.triggered.connect(self.setModeCP2)
		modeMenu.addAction(actMode)

		self.ti.setContextMenu(menu)

		if len(self.config.get('app', 'dspBbl')) > 0:
			self.actShowBuffer = QAction('Show action', self)
			self.actShowBuffer.triggered.connect(self.showBubbleTrigger)
			self.actShowBuffer.setShortcut(self.config.get('app', 'dspBbl'))

		self.ti.activated.connect(self.showBubble)

	@pyqtSlot('QString', 'QString')
	def notificationClosed(self, quid, body):
		try:
			self.activeNotifications.remove(body)
		except ValueError:
			pass

	@pyqtSlot()
	def notificationsToggled(self):
		muteText = 'Mute' if not self.config.getboolean('window', 'muted') else 'Unmute'
		self.actMuted.setText(muteText)

	@pyqtSlot()
	def toggleNotification(self):
		self.notification.model.toggleNotificationDisabled()

	@pyqtSlot()
	def settings(self):
		pzset = PruefziSettings(self.mainWindow, self.config)
		pzset.sigConfigChanged.connect(self.reloadConfig)
		pzset.show()
		#pzset.exec_()

	@pyqtSlot()
	def setModeOff(self):
		self.mode = Pruefzi.MODE_OFF
		self.updateMode()

	@pyqtSlot()
	def setModeC(self):
		self.mode = Pruefzi.MODE_C
		self.updateMode()

	@pyqtSlot()
	def setModeP(self):
		self.mode = Pruefzi.MODE_P
		self.updateMode()

	@pyqtSlot()
	def setModeP2(self):
		self.mode = Pruefzi.MODE_P2
		self.updateMode()

	@pyqtSlot()
	def setModeCP(self):
		self.mode = Pruefzi.MODE_CP
		self.updateMode()

	@pyqtSlot()
	def setModeCP2(self):
		self.mode = Pruefzi.MODE_CP2
		self.updateMode()

	@pyqtSlot()
	def setModeBubble(self):
		self.mode = Pruefzi.MODE_DSP
		self.updateMode()

	def updateMode(self):
		self.menuMode.setText('Mode: %s' % (Pruefzi.MODE_DESC[self.mode],))#
		if self.lastCalculation is not None:
			self.processPruefzi(self.lastCalculation, True)

	@pyqtSlot()
	def quit(self):
		sys.exit(0)

	@pyqtSlot(bool)
	def showBubbleTrigger(self, state):
		self.showBubble(0)

	@pyqtSlot('QSystemTrayIcon::ActivationReason')
	def showBubble(self, reason):
		if reason != 3:
			return

		if self.lastCalculation is None:
			self.notify.emit(
				APP_NAME, 'checkdigit', 'Check digit', 
				'Nothing in buffer which i can show you.', '',
				self.config.getfloat('app', 'bubbleTime')
			)
			Log.info('Want to show something from buffer.. but there is nothing!')
		elif str(self.lastCalculation) not in self.activeNotifications:
			self.notify.emit(
				APP_NAME, 'checkdigit', 'Check digit calculated!', 
				#str(self.lastCalculation), '%s/%s' % (self.lastCalculation.pz, self.lastCalculation.pz2),
				str(self.lastCalculation), '%s' % (self.lastCalculation.pz, ),
				self.config.getfloat('app', 'bubbleTime')
			)
			self.activeNotifications.append(str(self.lastCalculation))
			Log.info('check digit from buffer: %s' % (str(self.lastCalculation),))
		else:
			Log.info('Buffer is already shown...')

	def processPruefzi(self, pcalc, modeChange=False):
		pcalc.setResult(self.mode)
		if self.mode != Pruefzi.MODE_OFF:
			if pcalc.result == pcalc.original:
				self.lastNumber = '0'
			else:
				self.lastNumber = pcalc.result

			self.dontCheck = pcalc.result
			self.dontCheckOriginal = pcalc.cleared
			self.dontCheckUntil = datetime.datetime.now() + datetime.timedelta(seconds=1)
			if self.mode != Pruefzi.MODE_DSP or modeChange:
				if win32clipboard is None:
					# Tried everything,... the clipboard gets empty
					# when using the Qt5 ability.
					#self.clipboard.clear(mode=self.clipboard.Clipboard)
					#self.clipboard.setText(pcalc.result, mode=self.clipboard.Clipboard)
					r = Tk()
					r.withdraw()
					r.clipboard_clear()
					r.clipboard_append(pcalc.result)
					r.update()
					r.destroy()
					del(r)
				else:
					win32clipboard.OpenClipboard()
					win32clipboard.EmptyClipboard()
					win32clipboard.SetClipboardText(pcalc.result)
					win32clipboard.CloseClipboard()

			if str(pcalc) not in self.activeNotifications or self.activeNotifications.index(str(pcalc)) < (len(self.activeNotifications) - 1):
				self.lastCalculation = pcalc
				self.notify.emit(
					APP_NAME, 'checkdigit', 'Check digit calculated!', 
					str(self.lastCalculation), '%s' % (self.lastCalculation.pz, ),
					self.config.getfloat('app', 'bubbleTime')
				)
				self.activeNotifications.append(str(self.lastCalculation))
				Log.info('calculated check digit: %s' % (str(self.lastCalculation),))
			else:
				Log.info('Dropped duplicate number')

	@pyqtSlot()
	def dataChanged(self):
		try:
			text = str(self.clipboard.text()).strip()
			Log.info('Data changed. Got: %s' % (text,))
		except Exception as e:
			raise e

		# check if data is valid
		if len(text.strip()) <= 0:
			Log.info('Skipped due to empty request.')
			return

		if (text == self.dontCheck or text == self.dontCheckOriginal) and datetime.datetime.now() <= self.dontCheckUntil:
			Log.info('Skipped request for {:s} due to blockage.'.format(text))
			return

		# don't remove the chars, if it is an IP address (for now)
		skipProcess = False
		if self.config.getboolean('app', 'excludeIPs'):
			try:
				ipaddress.ip_address(text)
			except:
				skipProcess = False
			else:
				skipProcess = True
				Log.info('Skipped request for {:s} as its IP address.'.format(text))

		if not skipProcess:
			for f in self.config.get('app', 'charsToRemove'):
				text = text.replace(f, '')

			if len(text.strip()) < self.config.getint('app', 'minLength') \
			 or len(text.strip()) > self.config.getint('app', 'maxLength'):
				Log.info('Skipped request for {:s} as length does not match.'.format(text))
				return

			try:
				textIntText = int(text)
			except:
				return
			else:
				if text == self.lastNumber:
					Log.info('Skipped request for {:s} as copied number was already check last time.'.format(text))
					return

				pc = PruefziCalculation(str(self.clipboard.text()), text)
				# parse the data
				pc.parse()
				# process the data
				self.processPruefzi(pc)

def log_uncaught_exceptions(ex_cls, ex, tb):
	Log.exception(ex)
	Log.critical(''.join(traceback.format_tb(tb)))
	Log.critical('{0}: {1}'.format(ex_cls, ex))

	sys.__excepthook__(ex_cls, ex, tb) 

sys.excepthook = log_uncaught_exceptions

try:
	appmutex = clipmutex.singleinstance()
	if appmutex.aleradyrunning():
		Log.warning('Application already running!')
		sys.exit(0)
except:
	pass

app = QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)
ti = QSystemTrayIcon(QIcon('clip.png'))
pruefzi = Pruefzi(app, ti)

sys.exit(app.exec_())
