#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# @author Lukas Schreiner
#

import sys
import os.path
import subprocess
import shlex
import glob
import zipfile
import shutil
from cx_Freeze import setup, Executable

# compiling environment for Python 3.7
PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

# wrapper directory about project.
scriptDir = os.path.dirname(os.path.realpath(__file__))
buildDir = os.path.join(scriptDir, 'build')
distDir = os.path.join(scriptDir, 'dist')

def addToZip(zipFile, p):
	if os.path.isdir(p):
		for fls in glob.glob(os.path.join(p, '*'), recursive=True):
			addToZip(zipFile, fls)
	else:
		zipFile.write(p, os.path.relpath(p, buildDir))

files = [
	os.path.join(scriptDir, 'clip.png'),
	os.path.join(scriptDir, 'settings.ini')
]

# really dirty hack for Windows Server
if sys.platform == 'win32':
	# Platforms plugins
	pluginPath = os.path.join(
		os.path.dirname(sys.executable),
		'..\\Lib\\site-packages\\PyQt5\\Qt\\plugins\\platforms'
	)
	for f in glob.glob(pluginPath + '\\*.dll'):
		files.append((f, 'platforms\\' + os.path.basename(f)))
	# Image formats plugins
	pluginPath = os.path.join(
		os.path.dirname(sys.executable),
		'..\\Lib\\site-packages\\PyQt5\\Qt\\plugins\\imageformats'
	)
	for f in glob.glob(pluginPath + '\\*.dll'):
		files.append((f, 'imageformats\\' + os.path.basename(f)))

# convert ressource file!
cmd = 'pyrcc5 -o rc_data.py data.qrc'
exeCmd = shlex.split(cmd)
p = subprocess.Popen(exeCmd)
p.communicate()

setupName = 'Check Digit Calculator'
setupVersion = "2.5.3"
setupDescription = "Advanced Check Digit Calculator called Prüfzi"
setupPublisher = 'Lukas Schreiner'
setupPublisherMail = 'github@lschreiner.de'
setupUrl = 'https://lschreiner.de'
setupGuid = 'ADB5D781-5F0F-4CCB-AECF-D1CEF4ABB5C8'
setupIco = os.path.join(scriptDir, 'clip.ico')
files.append((setupIco, 'logo.ico'))

base = None
exeName = 'clip'
exeDebug = 'clip_debug'
if sys.platform == "win32":
	base = "Win32GUI"
	exeName = exeName + '.exe'
	exeDebug = exeDebug + '.exe'

clip = Executable(
	"clip.py",
	base = base,
	icon = setupIco,
	targetName = exeName,
	copyright = setupPublisher
)

clip_debug = Executable(
	"clip.py",
	base = None,
	icon = setupIco,
	targetName = exeDebug,
	copyright = setupPublisher
)

buildOpts = {
	'include_files': files,
	'zip_include_packages': [
		'PyQt5.QtNetwork', 'PyQt5.QtQml', 'PyQt5.QtSvg', 'PyQt5.QtQuick', \
		'PyQt5.QtQuickWidgets', 'tkinter'
	],
	'include_msvcr': True,
	'build_exe': os.path.join(buildDir, 'pruefzi-{:s}'.format(setupVersion))
}

# On windows, add the tkinter dll files.
if sys.platform == 'win32':
	buildOpts['include_files'].append(os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'))
	buildOpts['include_files'].append(os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'))

setup(
	name = setupName,
	version = setupVersion,
	description=setupDescription,
	author = setupPublisher,
	author_email = setupPublisherMail,
	url = setupUrl,
	options = {'build_exe': buildOpts},
	executables = [clip, clip_debug]
)

# inno setup?
if sys.platform == "win32":
	cmd = 'iscc \
	/DMyAppVersion="{version}" \
	/DMyAppName="{name}" \
	/DMyAppPublisher="{publisher}" \
	/DMyAppURL="{url}" \
	/DMyAppExeName="{exeName}" \
	/DbuildDirectory="{buildDirectory}" \
	/DMyAppGuid="{appGuid}" \
	inno_setup.iss'.format(
		version=setupVersion,
		name=setupName,
		publisher=setupPublisher,
		url=setupUrl,
		exeName=exeName,
		buildDirectory=os.path.join(buildDir, 'pruefzi-{:s}'.format(setupVersion)),
		appGuid=setupGuid
	)
	exeCmd = shlex.split(cmd)
	p = None
	try:
		p = subprocess.Popen(exeCmd)
	except FileNotFoundError:
		sys.stderr.write('No iscc builder available!\n')
	else:
		p.communicate()
		# copy setup file to build directory.
		srcSetupFile = os.path.join(scriptDir, 'Output', 'Pruefzi' + '_' + setupVersion + '_setup.exe')
		dstSetupFile = os.path.join(buildDir, 'Pruefzi' + '_' + setupVersion + '_setup.exe')
		if os.path.exists(srcSetupFile):
			shutil.copyfile(srcSetupFile, dstSetupFile)

# create dist file.
if not os.path.exists(distDir):
	try:
		os.makedirs(distDir, exist_ok=True)
	except:
		pass

distZipName = os.path.join(distDir, 'Pruefzi_{:s}.zip'.format(setupVersion))
zf = zipfile.ZipFile(distZipName, 'w', compression=zipfile.ZIP_DEFLATED)
for fls in glob.glob(os.path.join(buildDir, 'pruefzi-{:s}'.format(setupVersion), '*'), recursive=True):
	addToZip(zf, fls)

zf.close()
