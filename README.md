# Prüfzi
Prüfzi is a tool to calculate different kind of check digits (mainly MOD 10) whenever you copy some numbers. This tool will present you the check digit. Works under Linux as well as Windows.

## Author
Lukas Schreiner

## License
This code available under the New (3-clause) BSD license.
It contains work of Kabal (Notification system) from [equalsraf](https://github.com/equalsraf/kabal)