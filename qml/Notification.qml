import QtQuick 2.2

Rectangle {
	id: notification
	width: 300
	height: 125

	Style { id: style}
	color: 'transparent'

	Rectangle {

		id: container
		radius: 5
		anchors.margins: 5
		anchors.fill: parent
		color: style.backgroundColor2

		Image {
			id: iconImage
			source: icon
			width: 48
			height: 48
			fillMode: Image.PreserveAspectFit

			anchors.leftMargin: 4
			anchors.top: parent.top
			anchors.left: parent.left
		}

		Text {
			id: summaryText
			text: summary
			font.bold: true
			color: style.textColor
			elide: Text.ElideRight
			wrapMode: Text.Wrap

			anchors.left: iconImage.right
			anchors.right: parent.right
			anchors.top: parent.top
			anchors.topMargin: 4
			anchors.leftMargin: 8
			anchors.bottomMargin: 4
		}
		Text {
			id: bodyText
            text: body
            anchors.rightMargin: 3
			textFormat: Text.PlainText
			color: style.textColor
			//elide: Text.ElideRight
			wrapMode: Text.Wrap
			maximumLineCount: 5
			anchors.leftMargin: 6
			anchors.left: iconImage.right
			//anchors.right: parent.right
			anchors.right: checkDigitArea.left
			anchors.bottom: actionbox.top
			anchors.top: summaryText.bottom
		}
		Text {
			id: checkDigitArea
			height: 79
			font.pointSize: 64
            horizontalAlignment: Text.AlignHCenter
            textFormat: Text.PlainText
            color: style.textColor
            text: checkdigit
			anchors.left: bodyText.left
            anchors.leftMargin: 137
			anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 2
			anchors.top: parent.top
			anchors.topMargin: 2
			font.weight: Font.Bold
			font.letterSpacing: -5
		}

		MouseArea {
			anchors.fill: parent
            onClicked: {
				var dflidx = actionkeys.indexOf('default')
				if (dflidx == -1 ) {
					notificationModel.closeNotification(uid)
				} else {
					notificationModel.invokeAction(uid, actionkeys[dflidx])
				}
			}
		}

		Row {
			id: actionbox
			height: 16
			anchors.left: parent.left
			anchors.right: parent.right
			anchors.bottom: parent.bottom
			anchors.margins: 2
			spacing: 4

			Repeater {

				model: actionkeys
				Button { 
					label: actionnames[index]
					height: parent.height
					onClicked: notificationModel.invokeAction(uid, actionkeys[index])
				}
			}
		}


	} // Container

}
