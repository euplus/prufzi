#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QEvent
import logging

Log = logging.getLogger('pruefzi')

class DeclarativeView(QQuickView):
	mouseWheelUp = pyqtSignal()
	mouseWheelDown = pyqtSignal()

	def __init__(self, parent = None):
		super(QQuickView, self).__init__(parent)

	def screenCount(self):
		return QApplication.desktop().screenCount()

	def screenNumber(self):
		return QApplication.desktop().screenNumber()

	def screenGeometry(self):
		return QApplication.desktop().screenGeometry()

	def primaryScreen(self):
		return self.screenNumber() == QApplication.desktop().primaryScreen()

	def event(self, e):
		if e.type() == 10:
			print('I request it now... really...')
			self.requestActivate()
			self.raise_()
			#import pprint
			#methods = dir(self)
			#methods.sort()
			#pprint.pprint(methods)
			return True
		else:
			return super(QQuickView, self).event(e)

	def wheelEvent(self, ev):
		if ev.angleDelta().y() > 0:
			self.mouseWheelUp.emit()
		elif ev.angleDelta().y() < 0:
			self.mouseWheelDown.emit()
