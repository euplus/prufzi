#!/usr/bin/python
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTimer, pyqtSlot

class NotificationTimeout(QTimer):
	def __init__(self, model, uid, timeout):
		super(QTimer, self).__init__()

		self.model = model
		self.uid = uid

		self.setInterval(timeout)
		self.setSingleShot(True)
		self.timeout.connect(self.timedOut)

	@pyqtSlot()
	def timedOut(self):
		print('Timed out')
		self.model.closeNotification(self.uid, 1)

