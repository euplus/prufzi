# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'settings.ui'
#
# Created: Thu Apr  3 20:24:13 2014
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Settings(object):
    def setupUi(self, Settings):
        Settings.setObjectName(_fromUtf8("Settings"))
        Settings.resize(437, 208)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("clip.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Settings.setWindowIcon(icon)
        self.formLayout = QtGui.QFormLayout(Settings)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.labMinNr = QtGui.QLabel(Settings)
        self.labMinNr.setObjectName(_fromUtf8("labMinNr"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.labMinNr)
        self.labMaxNr = QtGui.QLabel(Settings)
        self.labMaxNr.setObjectName(_fromUtf8("labMaxNr"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.labMaxNr)
        self.labChRmv = QtGui.QLabel(Settings)
        self.labChRmv.setObjectName(_fromUtf8("labChRmv"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.labChRmv)
        self.labBblTme = QtGui.QLabel(Settings)
        self.labBblTme.setObjectName(_fromUtf8("labBblTme"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.labBblTme)
        self.labDefMode = QtGui.QLabel(Settings)
        self.labDefMode.setObjectName(_fromUtf8("labDefMode"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.labDefMode)
        self.spinMin = QtGui.QSpinBox(Settings)
        self.spinMin.setMinimum(1)
        self.spinMin.setProperty("value", 9)
        self.spinMin.setObjectName(_fromUtf8("spinMin"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.spinMin)
        self.spinMax = QtGui.QSpinBox(Settings)
        self.spinMax.setMinimum(1)
        self.spinMax.setProperty("value", 11)
        self.spinMax.setObjectName(_fromUtf8("spinMax"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.spinMax)
        self.charsToRemove = QtGui.QLineEdit(Settings)
        self.charsToRemove.setObjectName(_fromUtf8("charsToRemove"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.charsToRemove)
        self.bubbleTime = QtGui.QDoubleSpinBox(Settings)
        self.bubbleTime.setProperty("value", 10.0)
        self.bubbleTime.setObjectName(_fromUtf8("bubbleTime"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.bubbleTime)
        self.buttonBox = QtGui.QDialogButtonBox(Settings)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.buttonBox)
        self.comboBox = QtGui.QComboBox(Settings)
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.comboBox)
        self.labWarning = QtGui.QLabel(Settings)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.labWarning.setFont(font)
        self.labWarning.setStyleSheet(_fromUtf8("color:#ff0000"))
        self.labWarning.setObjectName(_fromUtf8("labWarning"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.labWarning)

        self.retranslateUi(Settings)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Settings.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Settings.reject)
        QtCore.QMetaObject.connectSlotsByName(Settings)
        Settings.setTabOrder(self.spinMin, self.spinMax)
        Settings.setTabOrder(self.spinMax, self.charsToRemove)
        Settings.setTabOrder(self.charsToRemove, self.bubbleTime)
        Settings.setTabOrder(self.bubbleTime, self.comboBox)
        Settings.setTabOrder(self.comboBox, self.buttonBox)

    def retranslateUi(self, Settings):
        Settings.setWindowTitle(_translate("Settings", "Check digit calculator - Settings", None))
        self.labMinNr.setText(_translate("Settings", "Minimum length of number:", None))
        self.labMaxNr.setText(_translate("Settings", "Maximum length of number:", None))
        self.labChRmv.setText(_translate("Settings", "Chars to remove:", None))
        self.labBblTme.setText(_translate("Settings", "Bubble time (in sec):", None))
        self.labDefMode.setText(_translate("Settings", "Default mode:", None))
        self.charsToRemove.setText(_translate("Settings", ",.", None))
        self.labWarning.setText(_translate("Settings", "You have to restart the Application!", None))

